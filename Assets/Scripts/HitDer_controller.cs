﻿using UnityEngine;
using System.Collections;

public class HitDer_controller : MonoBehaviour
{
	
	public HitDer_box _onff;

	// Use this for initialization
	void Start ()
	{
		//_rb.constraints = RigidbodyConstraints2D.FreezeAll;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnTriggerEnter2D (Collider2D other)
	{


		if (other.transform.tag == "hitDer_box") {

			_onff._onOff = true;
		} 
	}

	void OnTriggerStay2D (Collider2D other)
	{

		if (other.transform.tag == "hitDer_box") {

			_onff._onOff = true;

		} 
	}

	void OnTriggerExit2D (Collider2D another)
	{
		if (another.transform.tag == "hitDer_box") {

			_onff._onOff = false;
		}
	}
		
}
