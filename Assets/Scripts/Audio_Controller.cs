﻿using UnityEngine;
using System.Collections;

public class Audio_Controller : MonoBehaviour
{

	public AudioSource _audiS;

	public Player_Controller _playerCntr;

	void Awake ()
	{

		StartCoroutine ("WaitABit");

		DontDestroyOnLoad (gameObject);

		
	}

	// Use this for initialization
	void Start ()
	{
		_audiS.mute = false;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
	
		if (Input.GetKeyDown (KeyCode.M) && _audiS.mute == false) {
			_audiS.mute = true;
		} else if (Input.GetKeyDown (KeyCode.M) && _audiS.mute == true) {
			_audiS.mute = false;
		}

		if (_playerCntr._death == true) {
			_audiS.mute = true;
		}




	}

	IEnumerator WaitABit ()
	{
		yield return new WaitForSeconds (1);

		if (Input.GetKeyDown (KeyCode.Q) || Input.GetKeyDown (KeyCode.R)) {
			Destroy (gameObject);
		}

	}





}
