﻿using UnityEngine;
using System.Collections;

public class Fade_To_Dark : MonoBehaviour
{

	public Texture2D _fadeToDark;
	public float _fadeSpeed = 0.8f;

	private int drawInDept = -1000;
	private float alpha = 1.0f;
	private int fadeDir = -1;

	void OnGUI ()
	{
		alpha += fadeDir * _fadeSpeed * Time.deltaTime;
		alpha = Mathf.Clamp01 (alpha);

		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha);
		GUI.depth = drawInDept;
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), _fadeToDark);

	}

	public float BeginFadeIn (int direction)
	{
		fadeDir = direction;
		return(_fadeSpeed);
	}

	void OnLevelWasLoaded ()
	{
		BeginFadeIn (-1);
	}
}
