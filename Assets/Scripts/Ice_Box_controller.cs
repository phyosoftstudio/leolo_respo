﻿using UnityEngine;
using System.Collections;
using System;

public class Ice_Box_controller : MonoBehaviour
{

	public AudioSource _Audios;


	public AudioClip _WaterOnFire;

	public bool _iceDestroyd;

	public SpriteRenderer _iceBox;

	public GameObject _part_1;

	public GameObject _part_2;

	public BoxCollider2D _bc2d;


	// Use this for initialization
	void Start ()
	{
	
		_iceDestroyd = false;
	}


	void OnCollisionEnter2D (Collision2D other)
	{
		if (other.transform.tag == "Fire Box") {

			_iceDestroyd = true;

			_iceBox.enabled = false;
			_part_1.SetActive (false);

			_part_2.SetActive (false);

			_bc2d.enabled = false;

			AudioSource audio = GetComponent<AudioSource> ();

			audio.Play ();


			audio.clip = _WaterOnFire;

			audio.Play (); 

			StartCoroutine (Waitfor ());

			Destroy (other.gameObject);
			//Destroy (gameObject);

		}
		if (other.transform.tag == "Fire Wall") {
			_iceDestroyd = true;

			_iceBox.enabled = false;
			_part_1.SetActive (false);

			_part_2.SetActive (false);

			_bc2d.enabled = false;

			AudioSource audio = GetComponent<AudioSource> ();

			audio.Play ();


			audio.clip = _WaterOnFire;

			audio.Play (); 

			StartCoroutine (Waitfor ());

		}
	
	}

	IEnumerator Waitfor ()
	{
		AudioSource audio = GetComponent<AudioSource> ();

		audio.Play ();




		yield return new WaitForSeconds (audio.clip.length);

		Destroy (gameObject);
		audio.clip = _WaterOnFire;
		audio.Play ();






	}

	IEnumerator StartExp ()
	{
		AudioSource audio = GetComponent<AudioSource> ();

		audio.Play ();
		yield return new WaitForSeconds (audio.clip.length);
		audio.clip = _WaterOnFire;
		audio.Play ();
	}
		


}