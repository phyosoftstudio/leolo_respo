﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography.X509Certificates;

public class Box_Controller : MonoBehaviour
{

	public SpriteRenderer _Box_Sprite;
	public BoxCollider2D _Box_coll;
	public GameObject _Attached_FireBox;

	public AudioSource _audioSorce;

	public Boc_Controller _boc_script;

	// Use this for initialization
	void Start ()
	{


		_Box_Sprite.enabled = true;
		_Box_coll.isTrigger = false;

		_Attached_FireBox.SetActive (false);

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.transform.tag == "Fire Box" || other.transform.tag == "Fire Wall") {

			_audioSorce.Play ();

		
			_Attached_FireBox.SetActive (true);

			_Box_Sprite.enabled = false;
			_Box_coll.isTrigger = true;

			_boc_script.enabled = false;


		}

	}


}
