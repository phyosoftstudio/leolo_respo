﻿using UnityEngine;
using System.Collections;
using System.Security.Policy;
using System;

public class IceMud_Controller2 : MonoBehaviour
{
	
	public GameObject _IceBox;

	public Renderer _render;

	public AudioSource _AudioS;

	public MeshRenderer _meshRend;

	public SpriteRenderer _IceMud_sprite;

	public PolygonCollider2D _IceMud_polly;


	// Use this for initialization
	void Start ()
	{
		

		_meshRend.enabled = false;

		_IceMud_sprite.enabled = true;
		_IceMud_polly.enabled = true;

		_IceBox.SetActive (false);



	
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	void OnTriggerExit2D (Collider2D other)
	{

		if (other.tag == "Player") {


			_IceMud_polly.enabled = false; 
					
		

			_IceMud_sprite.enabled = false;
			_IceMud_polly.enabled = false;


			_meshRend.enabled = false;

			_IceBox.SetActive (true);






		}

	}
}
