﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

//using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;

public class Player_Controller : MonoBehaviour
{
	public GameObject _StartPos;

	public GameObject _tokenZone;

	public GameObject _Lock;

	public GameObject _Stair;

	public GameObject _iceSheeld;

	public CircleCollider2D _ciCol;

	public Rigidbody2D _rb;

	public Rigidbody2D _boxRid;

	public Game_Controller _GM;

	private Animator _anim;
	public AudioSource _audioController;

	public AudioClip _tokenOpenSound;
	public AudioClip _OpenDoor;
	public AudioClip _deathSound;

	public float xSpeed = 0.0f;

	public float ySpeed = 0.0f;

	public bool _moveUp = false;
	public bool _moveRight = false;
	public bool _moveLeft = false;
	public bool _moveDown = false;

	public bool _WalkUp = true;
	public bool _WalkDown = true;
	public bool _WalkLeft = true;
	public bool _WalkRight = true;

	public bool _death = false;

	public float _deathCount = 0.8f;



	// Use this for initialization
	void Start ()
	{
		GetComponent <Game_Controller> ();

		//GetComponent <AudioSource> ();

		_anim = GetComponent <Animator> ();

		gameObject.transform.position = _StartPos.transform.position;

		_moveUp = true;
		_moveRight = true;
		_moveLeft = true;
		_moveDown = true;

		_Lock.SetActive (true);
		_Stair.SetActive (false);

		_death = false;

		_iceSheeld.SetActive (false);
	


	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
			
		if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.W) && _moveUp == true) {

			//	_anim.Play ("PlauerGoUp");



			_rb.AddForce (Vector3.up * ySpeed * Time.deltaTime);

			_anim.Play ("WalkUp");

			_moveUp = true;

			_moveLeft = false;
			_moveRight = false;
			_moveDown = false;

			_WalkUp = true;

			_WalkLeft = false;
			_WalkRight = false;
			_WalkDown = false;

		} else if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A) && _moveLeft == true) {
			
			_rb.AddForce (Vector3.left * xSpeed * Time.deltaTime);

			_anim.Play ("WalkLeft");

			_moveLeft = true;

			_moveUp = false;
			_moveRight = false;
			_moveDown = false;

			_WalkLeft = true;

			_WalkUp = false;
			_WalkRight = false;
			_WalkDown = false;

		} else if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D) && _moveRight == true) {
			
			_rb.AddForce (Vector3.right * xSpeed * Time.deltaTime);

			_anim.Play ("WalkRight");

			_moveRight = true;

			_moveUp = false;
			_moveLeft = false;
			_moveDown = false;

			_WalkRight = true;

			_WalkUp = false;
			_WalkLeft = false;
			_WalkDown = false;

		} else if (Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.S) && _moveDown == true) {
			
			_rb.AddForce (Vector3.down * ySpeed * Time.deltaTime);

			_anim.Play ("WalkDown");

			_moveDown = true;

			_moveUp = false;
			_moveLeft = false;
			_moveRight = false;

			_WalkDown = true;

			_WalkUp = false;
			_WalkLeft = false;
			_WalkRight = false;
		



		} else {
			_moveUp = true;
			_moveRight = true;
			_moveLeft = true;
			_moveDown = true;




			if (_WalkDown == true) {

				_anim.Play ("Idle_Down");

				_WalkDown = false;


			} else if (_WalkUp == true) {

				_anim.Play ("Idle_Up");


				_WalkUp = false;

			} else if (_WalkLeft == true) {
				_anim.Play ("Idle");

				_WalkLeft = false;
			} else if (_WalkRight == true) {
				
				_anim.Play ("Idle_Right");
				_WalkRight = false;
			}

		}

		if (_Stair.active == false && _tokenZone.active == false) {
			
			AudioSource audio_1 = GetComponent<AudioSource> ();

			audio_1.Play ();

			audio_1.clip = _OpenDoor;
			audio_1.Play (); 
		}





//		_anim.SetFloat ("Speed", Mathf.Abs (_rb.velocity.x));
//	
//
//
//		if (_rb.velocity.x > 0) {
//			transform.localScale = new Vector3 (-1f, 1f, 1f);
//
//		} else if (_rb.velocity.x < 0) {
//			transform.localScale = new Vector3 (1f, 1f, 1f);
//
//		}




//

	}


	void OnCollisionStay2D (Collision2D other)
	{
		
		//		if (other.transform.tag == "Token_Zone") {
		//
		//			Destroy (other.gameObject);
		//
		//			//_ciCol.enabled = false;
		//
		//			_tokenZone.SetActive (false);
		//		}

		if (other.transform.tag == "Fire Box" || other.transform.tag == "Fire Wall") {

			Animator _anim = GetComponent <Animator> ();

			_death = true;


			_moveUp = false;
			_moveLeft = false;
			_moveRight = false;
			_moveDown = false;




			_anim.Play ("Death_Anim");

			StartCoroutine (WaitFor ());

		}
	}



	void OnTriggerStay2D (Collider2D other)
	{
		if (other.transform.tag == "Token_Zone") {

			_tokenZone.SetActive (false);
		
			if (_tokenZone.active == false) {
				AudioSource audio_2 = GetComponent<AudioSource> ();

				audio_2.Play (); 

				audio_2.clip = _tokenOpenSound;
				audio_2.Play (); 


				if (Application.loadedLevel == 29 && _tokenZone.active == false) {
					_iceSheeld.SetActive (true);
				}

			}






			Destroy (other.gameObject);
			//_ciCol.enabled = false;

		}
	}



	IEnumerator WaitFor ()
	{
		//_audioController.Play ();


		AudioSource audio = GetComponent<AudioSource> ();


		audio.Play ();

		audio.clip = _deathSound;
		audio.Play (); 



		yield return new WaitForSeconds (0.8f);
		_death = false;



		Application.LoadLevel (Application.loadedLevel);

		//	yield return new WaitForSeconds (0.1f);





	}





	void OnTriggerExit2D (Collider2D other)
	{


	}

	void OnTriggerEnter2D (Collider2D other)
	{
			
		if (other.transform.tag == "Token") {


			Destroy (other.gameObject);


//				_GM._lvlText.text = "L1-R2";
//
//				_GM._tokenCol.text = "T: 2";
			if (!_Stair) {
				Application.LoadLevel (Application.loadedLevel + 1);
			}
			if (Application.loadedLevel == 6) { 

//				_GM._lvlText.text = "L2-R1";
//
//				_GM._tokenCol.text = "6";

				_Lock.SetActive (false);
				_Stair.SetActive (true);


			} else if (Application.loadedLevel == 12) { 

				_Lock.SetActive (false);
				_Stair.SetActive (true);


			} else if (Application.loadedLevel == 18) { 
				
				_Lock.SetActive (false);
				_Stair.SetActive (true);


			} else if (Application.loadedLevel == 24) { 

				_Lock.SetActive (false);
				_Stair.SetActive (true);


			} else if (Application.loadedLevel == 30) { 

				_Lock.SetActive (false);
				_Stair.SetActive (true);
			}


		}
		if (_Stair) {

			if (other.transform.tag == "Stair" && Application.loadedLevel == 6) {

				Application.LoadLevel (Application.loadedLevel + 1);

				print ("Load levelS");
			} else if (other.transform.tag == "Stair" && Application.loadedLevel == 12) {

				Application.LoadLevel (Application.loadedLevel + 1);

				print ("Load level10");
			}
			if (other.transform.tag == "Stair" && Application.loadedLevel == 18) {

				Application.LoadLevel (Application.loadedLevel + 1);

				print ("Load level15");
			} else if (other.transform.tag == "Stair" && Application.loadedLevel == 24) {

				Application.LoadLevel (Application.loadedLevel + 1);

				print ("Load level20");
			} else if (other.transform.tag == "Stair" && Application.loadedLevel == 30) {

				Application.LoadLevel (Application.loadedLevel + 1);

				print ("Load level25");
			}
		}

	}



}
