﻿using UnityEngine;
using System.Collections;

public class Boc_Controller : MonoBehaviour
{

	public Rigidbody2D _rb;

	public HitDer_box _ONOFF;


	// Use this for initialization
	void Start ()
	{
		_rb.constraints = RigidbodyConstraints2D.FreezeAll;

		_ONOFF._onOff = false;


	}



	//	void OnTriggerEnter2D (Collider2D other)
	//	{
	//
	//
	//
	//		if (other.transform.tag == "HitDer") {
	//
	//
	//
	//		}
	//	}

	void OnTriggerEnter2D (Collider2D other)
	{
		
		if (other.transform.tag == "HitDer" && _ONOFF._onOff == true) {
			
			_rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
		} 
	}

	void OnTriggerStay2D (Collider2D other)
	{

		if (other.transform.tag == "HitDer" && _ONOFF._onOff == true) {

			_rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
		} 
	}


	void OnTriggerExit2D (Collider2D another)
	{
		if (another.transform.tag == "HitDer" && _ONOFF._onOff == false) {

			_rb.constraints = RigidbodyConstraints2D.FreezeAll;



		}
	}
}
