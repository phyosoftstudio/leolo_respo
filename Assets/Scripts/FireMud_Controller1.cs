﻿using UnityEngine;
using System.Collections;
using System.Security.Policy;
using System;

public class FireMud_Controller1 : MonoBehaviour
{

	public BoxCollider2D _box2d;
	//public BoxCollider2D _ToDeep;

	public GameObject _FireBox;

	public Renderer _render;

	public AudioSource _AudioS;

	public MeshRenderer _meshRend;

	public SpriteRenderer _FireMud_sprite;

	public PolygonCollider2D _FireMud_polly;


	// Use this for initialization
	void Start ()
	{
		

		_box2d.isTrigger = true;
		_meshRend.enabled = false;

		_FireMud_sprite.enabled = true;
		_FireMud_polly.enabled = true;

		_FireBox.SetActive (false);



	
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	void OnTriggerExit2D (Collider2D other)
	{

		if (other.tag == "Player") {


			_AudioS.Play ();

			_box2d.enabled = false; 
					
			_render.material.color = Color.magenta;
			_meshRend.enabled = false;

			_FireMud_sprite.enabled = false;
			_FireMud_polly.enabled = false;

			_FireBox.SetActive (true);






		}

	}
}
