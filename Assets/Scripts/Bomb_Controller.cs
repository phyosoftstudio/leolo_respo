﻿using UnityEngine;
using System.Collections;

public class Bomb_Controller : MonoBehaviour
{

	public SpriteRenderer _explosion;
	public SpriteRenderer _Bomb;
	public AudioClip _explosionSound;

	// Use this for initialization
	void Start ()
	{
		_Bomb.enabled = true;
		_explosion.enabled = false;

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnCollisionEnter2D (Collision2D other)
	{
		if (other.transform.tag == "Stone") {
			

			_Bomb.enabled = false;
			_explosion.enabled = true;

			AudioSource audio = GetComponent<AudioSource> ();

			audio.Play ();


			audio.clip = _explosionSound;

			audio.Play (); 

			StartCoroutine (Waitfor ());


			Destroy (other.gameObject);
			//Destroy (gameObject);
		}

		if (other.transform.tag == "Fire Box" || other.transform.tag == "Fire Wall") {



			_Bomb.enabled = false;
			_explosion.enabled = true;

			AudioSource audio = GetComponent<AudioSource> ();

			audio.Play ();


			audio.clip = _explosionSound;

			audio.Play (); 

			StartCoroutine (Waitfor ());


		}
	}

	IEnumerator Waitfor ()
	{


		yield return new WaitForSeconds (0.1f);

		Destroy (gameObject);
			

	}

	IEnumerator StartExp ()
	{
		AudioSource audio = GetComponent<AudioSource> ();

		audio.Play ();
		yield return new WaitForSeconds (audio.clip.length);
		audio.clip = _explosionSound;
		audio.Play ();
	}


}
